import setContent from './setContent.js';

const items = ['ABOUT', 'SETTINGS', 'OPTION1', 'OPTION2', 'OPTION3', 'OPTION4', 'OPTION5', 'OPTION6']
const navBar = document.getElementById('custom-nav-bar')

const dotsMenu = document.createElement('div')
dotsMenu.className = 'dots-menu'
dotsMenu.id = 'dots-menu'

const dots = document.createElement('div')
dots.innerHTML = '<img src="src/images/1x/baseline_more_horiz_black_18dp.png">'
dots.className = 'nav-item'
dots.addEventListener('click', toggleDotMenu)

function toggleDotMenu(){
    if (navBar.querySelector('.dots-menu')) {
        navBar.removeChild(dotsMenu)
    } else {
        navBar.appendChild(dotsMenu)
    }
}

function createNavBar() {
    
    navBar.innerHTML = ''
    dotsMenu.innerHTML = ''

    let dotFlag = false;
    
    items.forEach((item) => {
        navBar.appendChild(dots)
        let currentHeight = navBar.scrollHeight

        let newItem = document.createElement('div')
        newItem.innerHTML = item
        newItem.onclick = function() { setContent(item.toLowerCase()) }
        newItem.className = 'nav-item'
        navBar.appendChild(newItem)

        if (navBar.scrollHeight > currentHeight && currentHeight !== 0) {
            dotFlag = true
        }

        if( dotFlag ) {
            navBar.removeChild(newItem)
            dotsMenu.appendChild(newItem)
        } else {
            navBar.removeChild(dots)
        }
    })

}

createNavBar()

function checkOverflow(){
   createNavBar();
}

window.addEventListener('resize', checkOverflow)

document.addEventListener('click', function (event) {
    var isClickInside = dotsMenu.contains(event.target) || dots.contains(event.target);

    if (!isClickInside) {
        dotsMenu.remove()
    }
});