import fillProfile from './fillProfile.js'
import profileData from './profileData.json'

/**
 * Saves 'value' into profileData json 'key' attribute  
 * 
 * @param {string} key 
 * @param {string} value 
 */
export default function saveProfileDataAttribute(key, value) {
    profileData[key] = value
    fillProfile(profileData)
    fillProfile(profileData, true)
}