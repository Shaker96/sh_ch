import aboutComponent from '../html/about.html'
import fillProfile from './fillProfile.js'
import profileData from './profileData.json'

const components = {
    'about': aboutComponent,
    'settings': '<h2>Settings</h2>',
    'option1': '<h2>Option 1</h2>',
    'option2': '<h2>Option 2</h2>',
    'option3': '<h2>Option 3</h2>',
    'option4': '<h2>Option 4</h2>',
    'option5': '<h2>Option 5</h2>',
    'option6': '<h2>Option 6</h2>',
}

/**
 * loads the html component into the content div 
 * 
 * @param {string} componentKey 
 */
export default function setContent(componentKey) {
    const contentDiv = document.getElementById("content-container")
    const component = components[componentKey]
    contentDiv.innerHTML = component
    if (componentKey === 'about')
        fillProfile(profileData, true)
}