import popup from '../html/popup.html'
import profileData from './profileData.json'
import saveProfileDataAttribute from './saveProfileDataAttribute.js'
import closePopup from './closePopup.js'

const labels = {
    name: "FULLNAME",
    website: "WEBSITE",
    phone: "PHONE NUMBER",
    address: "CITY, STATE & ZIP"
}

const popupComponent = String(popup)

/**
 * creates popup container to edit the input in profileData with 'key'
 * 
 * @param {html caller element} elem 
 * @param {string} key 
 */
export default function createPopup(elem, key) {
    let currentPopup = document.getElementById('popup')
    if(currentPopup)
        currentPopup.remove()
    let parent = elem.parentElement    
    parent.innerHTML += popupComponent
    document.getElementById('edit-label').innerHTML = labels[key]
    document.getElementById('edit-input').value = profileData[key]
    var saveBtn = document.getElementById('edit-save');
    saveBtn.onclick = function (event) {
        const newValue = document.getElementById('edit-input').value
        saveProfileDataAttribute(key, newValue)
        closePopup()
    }
}