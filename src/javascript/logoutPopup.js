let logoutBtn = document.getElementById('logout')

let popup = document.createElement('div')
popup.className = 'logout-popup'
popup.id = 'logout-popup'

let popupText = document.createTextNode('Log-out')
popupText.className = 'text'

popup.appendChild(popupText)

document.addEventListener('click', function (event) {
    var isClickInside = popup.contains(event.target) || logoutBtn.contains(event.target);

    if (!isClickInside) {
        popup.remove()
    }
});

logoutBtn.addEventListener('click', function (event) {
    console.log('event')
    this.parentElement.appendChild(popup)
})
