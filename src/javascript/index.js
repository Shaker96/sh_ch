import '../sass/styles.scss'
import '../sass/styles-mobile.scss'
import '../sass/materialInput.scss'
import '../sass/tooltip.scss'
import '../sass/logout-popup.scss'
import '../sass/three-dots.scss'
import fillProfile from './fillProfile.js'
import profileData from './profileData.json'
import setContent from './setContent.js'
import createPopup from './createPopup.js'
import closePopup from './closePopup.js'
import getAllInputs from './getAllInputs.js'
import getProfileDataValues from './getProfileDataValues.js'
import './createTooltip.js'
import './logoutPopup.js'
import './createNavBar.js'

fillProfile(profileData) // initial profile hydratation

export {
    setContent,
    fillProfile,
    createPopup,
    closePopup,
    getAllInputs,
    getProfileDataValues
}