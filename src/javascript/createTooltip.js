const { default: fillProfile } = require("./fillProfile")

let followersDiv = document.getElementById('followers')

let tooltip = document.createElement('div')
tooltip.className = 'followers-tooltip'
tooltip.id = 'tooltip'

let tooltipText = document.createTextNode('Follow')
tooltipText.className = 'text'

tooltip.appendChild(tooltipText)

followersDiv.addEventListener('mouseenter', function (event) {
    this.appendChild(tooltip)
})

followersDiv.addEventListener('mouseleave', function (event) {
    this.removeChild(tooltip)
})