/**
 * Closes input popup
 */
export default function closePopup() {
    document.getElementById('popup').remove()
}