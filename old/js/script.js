const components = {
    'about': '<h1>About</h1>',
    'settings': '<h1>Settings</h1>',
    'option1': '<h1>Option 1</h1>',
    'option2': '<h1>Option 2</h1>',
    'option3': '<h1>Option 3</h1>'
};

const setContent = (componentKey) => {
    const contentDiv = document.getElementById("content-container")
    const component = components[componentKey]
    contentDiv.innerHTML = component
};